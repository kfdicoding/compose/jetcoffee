package learn.dicoding.jetcoffee.ui.main.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.jetcoffee.R
import learn.dicoding.jetcoffee.ui.components.SearchBar
import learn.dicoding.jetcoffee.ui.theme.JetCoffeeTheme

@Composable
fun Banner(
    modifier: Modifier = Modifier
){
    Box(
        modifier = modifier
    ){
        Image(
            painter = painterResource(id = R.drawable.banner),
            contentDescription = "Banner Image",
            contentScale = ContentScale.Crop,
            modifier = Modifier.height(160.dp)
        )
        SearchBar()
    }
}

@Preview
@Composable
fun BannerPreview(){
    JetCoffeeTheme {
        Banner()
    }
}
