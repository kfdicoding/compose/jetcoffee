package learn.dicoding.jetcoffee.ui.main.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.jetcoffee.model.dummyCategory
import learn.dicoding.jetcoffee.ui.components.CategoryItem
import learn.dicoding.jetcoffee.ui.components.SectionText
import learn.dicoding.jetcoffee.ui.theme.JetCoffeeTheme


@Composable
fun CategorySection(
    modifier: Modifier = Modifier
){
    Column {
        SectionText(title = "Mau ngopi Apa Hari ini?")
        LazyRow(
            contentPadding = PaddingValues(horizontal = 16.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            modifier = modifier,
        ){
            items(dummyCategory, key = {it.textCategory}) {category->
                CategoryItem(category)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CategorySectionPreview(){
    JetCoffeeTheme {
        CategorySection()
    }
}
