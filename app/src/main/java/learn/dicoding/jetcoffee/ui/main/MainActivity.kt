package learn.dicoding.jetcoffee.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.jetcoffee.R
import learn.dicoding.jetcoffee.model.dummyBestSellerMenu
import learn.dicoding.jetcoffee.model.dummyMenu
import learn.dicoding.jetcoffee.ui.main.components.Banner
import learn.dicoding.jetcoffee.ui.main.components.BottomBar
import learn.dicoding.jetcoffee.ui.main.components.CategorySection
import learn.dicoding.jetcoffee.ui.main.components.MenuSection
import learn.dicoding.jetcoffee.ui.theme.JetCoffeeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JetCoffeeApp()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun JetCoffeeApp(){
    JetCoffeeTheme {
        // A surface container using the 'background' color from the theme
        Scaffold(
            bottomBar = { BottomBar() },
        ){padding->
            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .padding(padding)
                    .padding(bottom = 24.dp)
            ) {
                Banner()
                CategorySection()
                MenuSection(title = stringResource(R.string.section_favorite_menu), listMenu = dummyMenu)
                MenuSection(title = stringResource(R.string.section_best_seller_menu), listMenu = dummyBestSellerMenu)
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun JetCoffeeAppPreview(){
    JetCoffeeApp()
}
