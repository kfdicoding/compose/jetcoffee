package learn.dicoding.jetcoffee.ui.main.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import learn.dicoding.jetcoffee.R
import learn.dicoding.jetcoffee.model.Menu
import learn.dicoding.jetcoffee.model.dummyBestSellerMenu
import learn.dicoding.jetcoffee.ui.components.MenuItem
import learn.dicoding.jetcoffee.ui.components.SectionText
import learn.dicoding.jetcoffee.ui.theme.JetCoffeeTheme

@Composable
fun MenuSection(
    title: String,
    listMenu: List<Menu>,
    modifier: Modifier = Modifier
){
    Column(
        modifier = modifier.padding(top = 8.dp)
    ) {
        SectionText(title)
        LazyRow(
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            contentPadding = PaddingValues(horizontal = 16.dp),
        ){
            items(listMenu, key = {it.title} ){menu->
                MenuItem(menu)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun MenuSectionPreview(){
    JetCoffeeTheme {
        MenuSection(
            title = stringResource(id = R.string.section_favorite_menu),
            listMenu = dummyBestSellerMenu
        )
    }
}
