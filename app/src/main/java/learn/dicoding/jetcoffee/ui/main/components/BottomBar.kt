package learn.dicoding.jetcoffee.ui.main.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountBox
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import learn.dicoding.jetcoffee.R
import learn.dicoding.jetcoffee.model.BottomBarItem
import learn.dicoding.jetcoffee.ui.theme.JetCoffeeTheme


@Composable
fun BottomBar(
    modifier: Modifier = Modifier
){
    NavigationBar(
        modifier = modifier
    ) {
        val navigationItems = listOf(
            BottomBarItem(
                title = stringResource(id = R.string.menu_home),
                icon = Icons.Default.Home
            ),
            BottomBarItem(
                title = stringResource(id = R.string.menu_favorite),
                icon = Icons.Default.Favorite
            ),
            BottomBarItem(
                title = stringResource(id = R.string.menu_profile),
                icon = Icons.Default.AccountCircle
            ),
        )

        navigationItems.map {menu->
            NavigationBarItem(
                icon = {
                   Icon(
                       imageVector = menu.icon,
                       contentDescription = null
                   )
                },
                label = {
                    Text(menu.title)
                },
                selected = menu.title == navigationItems[0].title,
                onClick = {}
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun BottomBarPreview(){
    JetCoffeeTheme {
        BottomBar()
    }
}
